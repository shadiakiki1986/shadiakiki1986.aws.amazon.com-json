# shadiakiki1986.aws.amazon.com-json

[Open-source infrastructure](https://opensourceinfra.org/) of my personal account on AWS. Exported as JSON files.

Posted on reddit: [Is open-source infrastructure safe?](https://www.reddit.com/r/aws/comments/cn81my/is_opensource_infrastructure_safe/)


## How to replicate

This repo was generated with [git-remote-aws](https://gitlab.com/autofitcloud/git-remote-aws) using the following commands

```
TMPDIR=`mktemp -d`
cd $TMPDIR
git init

git remote add ec2_descInstances  aws+ec2::/describe-instances?fulldata=false
git remote add cw_getMetricData   aws+cw::/get-metric-data
git remote add cw_listMetrics     aws+cw::/list-metrics

git fetch --all
```

Here's the pulled directory structure

```
> tree
.
├── LICENSE
├── README.md
└── aws.amazon.com
    └── us-west-2
        ├── cw_getMetricData
        │   ├── CPUUtilization - daily - 90 days - Average
        │   │   ├── i-02432bc7.json
        │   │   ├── i-069a7808addd143c7.json
        │   │   ├── i-08c802de5accc1e89.json
        │   │   ├── i-09e73607b0fa5e314.json
        │   │   ├── i-0e2662888859c5507.json
        │   │   ├── i-0fb05d874895a05ec.json
        │   │   ├── i-34ca2fc2.json
        │   │   └── i-e1ca46eb.json

...

        ├── cw_listMetrics
        │   ├── i-02432bc7.json
        │   ├── i-069a7808addd143c7.json
        │   ├── i-08c802de5accc1e89.json
        │   └── i-34ca2fc2.json
        └── ec2_describeInstances
            ├── i-02432bc7.json
            ├── i-069a7808addd143c7.json
            ├── i-08c802de5accc1e89.json
            ├── i-09e73607b0fa5e314.json
            ├── i-0e2662888859c5507.json
            ├── i-0fb05d874895a05ec.json
            ├── i-34ca2fc2.json
            └── i-e1ca46eb.json
```

Proceed by pushing to the git remote

```
git add aws.amazon.com
git commit -m 'first commit'

git remote add origin git@gitlab.com:shadiakiki1986/shadiakiki1986.aws.amazon.com-json.git
git push -u origin master
```
